# PiRDAC - Pi Rfid Door Access Control #

* Raspberry Pi
* Wiegand RFID 
* Redis Pub/Sub
* .ini file with card data (ID, active time, active days)

## pirdac_wiegand_reader ##
- reads wiegand RFID card codes via gpio interrupts
- submits correct data to redis

## pirdac_control ##
- subscribe to redis and waits for wiegand card codes
- when a code is received, checks if code is authorized based on users.ini
- if code is authorized calls pirdac_opendor

## pirdac_opendoor ##
- triggers a relay to open a door via gpio

## Diagram ##
![Diagram](diagram.png)
