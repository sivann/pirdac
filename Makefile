CC = gcc
CFLAGS = -I. -Wall -O
INCREDIS = -I/usr/local/include/hiredis
DEPS = pirdac.h
LIBS = -lpthread -lrt 
LIBSWPI = -lwiringPi
LIBREDIS = -lhiredis

binaries=pirdac_wiegand_reader pirdac_control pirdac_opendoor

all: $(binaries)

pirdac_wiegand_reader: pirdac_wiegand_reader.c
	gcc -o $@ $^ $(CFLAGS) $(INCREDIS) $(LIBS) $(LIBREDIS) $(LIBSWPI)

pirdac_control: pirdac_control.c  minIni.c
	gcc -o $@ $^ $(CFLAGS)  $(INCREDIS) $(LIBS) $(LIBREDIS)

pirdac_opendoor: pirdac_opendoor.c
	gcc -o $@ $^ $(CFLAGS) $(LIBS)  $(LIBSWPI)

clean:
	rm -f *.o $(binaries)
