/*
 * Wiegand data reader and Door access control with Raspberry Pi
 * https://bitbucket.org/sivann/pirdac/overview
 *
 * Tested with RPi Model B rev2, and an 26bit Wiegand Cards
 * Spiros Ioannou 2017
 *
 */

/* Wiegand Parser
 * 1) Parses Wiegand ID
 * 2) Publishes ID to REDIS
 * Compile with: gcc pirdac.c   -lwiringPi -lpthread -lrt  -Wall -o wiegand_rpi -O
 * 
 */

/*
 * This is interrupt drivern, no polling occurs. 
 * After each bit is read, a timeout is set. 
 * If timeout is reached read code is evaluated for correctness.
 *
 * Wiegand Bits:
 * pFFFFFFFFNNNNNNNNNNNNNNNNP
 * p: even parity of F-bits and leftmost 4 N-bits
 * F: Facility code
 * N: Card Number
 * P: odd parity of rightmost 12 N-bits
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <memory.h>
#include <stdint.h>
#include <sys/time.h>
#include <signal.h>
#include <wiringPi.h>
#include <hiredis.h>
#include <errno.h>
#include <sys/types.h>
#include <syslog.h>



#include <pirdac.h>

/* Defaults, change with command-line options */
#define D0_PIN 0
#define D1_PIN 2

#define WIEGANDMAXBITS 40

/* Set some timeouts */

/* Wiegand defines:
 * a Pulse Width time between 20 μs and 100 μs (microseconds)
 * a Pulse Interval time between 200 μs and 20000 μsec
 */

/* Each bit takes 4-6 usec, so all 26 bit sequence would take < 200usec */
#define WIEGAND_BIT_INTERVAL_TIMEOUT_USEC 20000 /* interval between bits, typically 1000us */

char logmsg[512];
struct itimerval it_val;
struct sigaction sa;

struct wiegand_data {
    unsigned char p0, p1;       //parity 0 , parity 1
    uint8_t facility_code;
    uint16_t card_code;
    uint32_t full_code;;
    int code_valid;
    unsigned long bitcount;     // bits read
} wds;

struct option_s {
    int d0pin;
    int d1pin;
    int debug;
	char reader_id[64];
} options;

void show_code();
void wiegand_sequence_reset();
void reset_timeout_timer(long usec);
void gpio_init(int d0pin, int d1pin);
void redis_publish(char * channel, char * message) ;
int redis_connect_loop() ;

static struct timespec wbit_tm; //for debug

/* Timeout from last bit read, sequence may be completed or stopped */
void wiegand_timeout() {
	char str[128];

    if (options.debug)
        fprintf(stderr, "wiegand_timeout()\n");
    wiegand_sequence_reset();
    show_code();

	if (wds.code_valid) {
		sprintf(str, "%d", wds.full_code);
		snprintf(logmsg, sizeof(logmsg), "Got valid code: Facility code: %d, Card code: %d, full code: %d",wds.facility_code, wds.card_code, wds.full_code);
		syslog (LOG_NOTICE,"%s",logmsg);
		redis_publish(options.reader_id, str);
	}
	else {
		snprintf(logmsg, sizeof(logmsg), "Got invalid code, bitcount:%ld",wds.bitcount);
		syslog (LOG_ERR,"%s",logmsg);
	}
}

void show_code() {
    printf("\n");
    printf("*** Code Valid: %d\n", wds.code_valid);
    printf("*** Facility code: %d(dec) 0x%X\n", wds.facility_code,
           wds.facility_code);
    printf("*** Card code: %d(dec) 0x%X\n", wds.card_code, wds.card_code);
    printf("*** Full code: %d\n", wds.full_code);
    printf("*** Parity 0:%d Parity 1:%d\n", wds.p0, wds.p1);
    fflush(stdout);
}

int setup_wiegand_timeout_handler() {
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = wiegand_timeout;;
    //sa.sa_flags = SA_SIGINFO;

    if (options.debug)
        fprintf(stderr, "\n");

    if (sigaction(SIGALRM, &sa, NULL) < 0) {
        perror("sigaction");
        return 1;
    };
    return 0;
}

/* Parse Wiegand 26bit format 
 * Called wherever a new bit is read
 * bit: 0 or 1
 */
void add_bit_w26(int bit) {
    static int parity0 = 0;
    static int parity1 = 0;

    //Parity calculation
    if (wds.bitcount == 0) {
        parity0 = 0;
        parity1 = 0;
    }
    if (wds.bitcount > 0 && wds.bitcount <= 12) {
        parity0 += bit;
    }
    else if (wds.bitcount >= 13 && wds.bitcount <= 24) {
        parity1 += bit;
    }
    //Code calculation
    if (wds.bitcount == 0) {
        /* Reset */
        wds.code_valid = 0;
        wds.facility_code = 0;
        wds.card_code = 0;
        wds.p0 = bit;           //parity
    }
    else if (wds.bitcount <= 8) {
        wds.facility_code <<= 1;
        if (bit)
            wds.facility_code |= 1;
    }
    else if (wds.bitcount < 25) {
        wds.card_code <<= 1;
        if (bit)
            wds.card_code |= 1;
    }
    else if (wds.bitcount == 25) {
        wds.p1 = bit;
        wds.full_code = wds.facility_code;
        wds.full_code = wds.full_code << 16;
        wds.full_code += wds.card_code;

        wds.code_valid = 1;
        //check parity
        if ((parity0 % 2) != wds.p0) {
            wds.code_valid = 0;
            if (options.debug) {
                fprintf(stderr, "Incorrect even parity bit (leftmost)\n");
            }
        }
        else if (!(parity1 % 2) != wds.p1) {
            wds.code_valid = 0;
            if (options.debug) {
                fprintf(stderr, "Incorrect odd parity bit (rightmost)\n");
            }
        }

    }
    else if (wds.bitcount > 25) {
        wds.code_valid = 0;
        wiegand_sequence_reset();

    }

    if (wds.bitcount < WIEGANDMAXBITS) {
        wds.bitcount++;
    }

}

unsigned long get_bit_timediff_ns() {
    struct timespec now, delta;
    unsigned long tdiff;

    clock_gettime(CLOCK_MONOTONIC, &now);
    delta.tv_sec = now.tv_sec - wbit_tm.tv_sec;
    delta.tv_nsec = now.tv_nsec - wbit_tm.tv_nsec;

    tdiff = delta.tv_sec * 1000000000 + delta.tv_nsec;

    return tdiff;

}

void d0_pulse(void) {
    reset_timeout_timer(WIEGAND_BIT_INTERVAL_TIMEOUT_USEC);     //timeout waiting for next bit
    if (options.debug) {
        fprintf(stderr, "Bit:%02ld, Pulse 0, %ld us since last bit\n",
                wds.bitcount, get_bit_timediff_ns() / 1000);
        clock_gettime(CLOCK_MONOTONIC, &wbit_tm);
    }
    add_bit_w26(0);
}

void d1_pulse(void) {
    reset_timeout_timer(WIEGAND_BIT_INTERVAL_TIMEOUT_USEC);     //timeout waiting for next bit
    if (options.debug) {
        fprintf(stderr, "Bit:%02ld, Pulse 1, %ld us since last bit\n",
                wds.bitcount, get_bit_timediff_ns() / 1000);
        clock_gettime(CLOCK_MONOTONIC, &wbit_tm);
    }
    add_bit_w26(1);
}

void wiegand_sequence_reset() {
    wds.bitcount = 0;
}

/* timeout handler, should fire after bit sequence has been read */
void reset_timeout_timer(long usec) {
    it_val.it_value.tv_sec = 0;
    it_val.it_value.tv_usec = usec;

    it_val.it_interval.tv_sec = 0;
    it_val.it_interval.tv_usec = 0;

    if (setitimer(ITIMER_REAL, &it_val, NULL) == -1) {
		snprintf(logmsg, sizeof(logmsg), "setitimer:%s",strerror(errno));
		syslog (LOG_ERR,"%s",logmsg);
    }
}

void gpio_init(int d0pin, int d1pin) {

    wiringPiSetup();
    pinMode(d0pin, INPUT);
    pinMode(d1pin, INPUT);

    wiringPiISR(d0pin, INT_EDGE_FALLING, d0_pulse);
    wiringPiISR(d1pin, INT_EDGE_FALLING, d1_pulse);

}

void show_usage(char *pname) {
    printf("PIRDAC - Parse Wiegand and publish to Redis (https://bitbucket.org/sivann/pirdac)\n");
    printf("Usage: %s [-d] [-0 D0-pin] [-1 D1-pin] [-i id]\n",pname);
    printf("\t-d\t\tdebug\n");
    printf("\t-0\t\tGPIO pin for data0 pulse\n");
    printf("\t-1\t\tGPIO pin for data1 pulse\n");
    printf("\t-i\t\tReader id. Used also as redis channel to publish. Defaults to reader-<D0> where D0 the GPIO pin number\n");
    printf("Check http://wiringpi.com/pins for WiringPi pin numbers\n");
    printf("\n");
}

int main(int argc, char *argv[]) {
    int opt;

    /* defaults */
    options.d0pin = D0_PIN;
    options.d1pin = D1_PIN;
    options.debug = 0;
    sprintf(options.reader_id,"reader-%d", D0_PIN);

    /* Parse Options */
    while ((opt = getopt(argc, argv, "d0:1:")) != -1) {
        switch (opt) {
        case 'd':
            options.debug++;
            break;
        case '0':
            options.d0pin = atoi(optarg);
            break;
        case '1':
            options.d1pin = atoi(optarg);
            break;
        case 'i':
			snprintf(options.reader_id, sizeof(options.reader_id), "reader-%s",optarg);
            //strncpy(options.reader_id, optarg, sizeof(options.reader_id));
            break;
        case 'h':
            show_usage(argv[0]);
            exit(0);
            break;
        default:               /* '?' */
            show_usage(argv[0]);
            exit(EXIT_FAILURE);
        }
    }
	
	openlog (argv[0], LOG_PID|LOG_PERROR , LOG_AUTH);
	syslog (LOG_INFO, "Started by uid %d", getuid () );

    setup_wiegand_timeout_handler();
    gpio_init(options.d0pin, options.d1pin);
    wiegand_sequence_reset();
	redis_connect_loop();

    while (1) {
        pause();
		syslog (LOG_INFO,"After pause()");
    }
}


redisReply *reply;
redisContext *redis_context=NULL;

int redis_connect_loop() {
	int connected=0;

	if (redis_context != NULL) {
		redisFree(redis_context);
	}
	while (!connected) {
		redis_context = redisConnect("127.0.0.1", 6379);
		if (redis_context == NULL || redis_context->err) {
			if (redis_context) {
				snprintf(logmsg, sizeof(logmsg), "Error connecting: %s\n", redis_context->errstr);
				syslog (LOG_ERR,"%s",logmsg);

				redisFree(redis_context);
			} else {
				snprintf(logmsg,sizeof(logmsg),"Can't allocate redis context\n");
				syslog (LOG_ERR,"%s",logmsg);
			}
			snprintf(logmsg, sizeof(logmsg),"connect failed, retrying..\n");
			syslog (LOG_ERR,"%s",logmsg);
			sleep (1);
		}
		else {
			if (options.debug) {
				fprintf(stderr, "Connected to redis\n");
			}
			connected=1;
		}
	}
	return 0;
}

void redis_publish(char * channel, char * message) {

	if (options.debug)
		fprintf(stderr, "Publishing message [%s] to redis channel [%s]\n",message, channel);

	reply = redisCommand(redis_context, "PUBLISH %s \"%s\"", channel,message);
	if (!reply) {
		redis_connect_loop();
		reply = redisCommand(redis_context, "PUBLISH %s \"%s\"", channel,message);
	}
	freeReplyObject(reply);
}

