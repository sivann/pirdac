
/*
 * Wiegand data reader and Door access control with Raspberry Pi
 * https://bitbucket.org/sivann/pirdac/overview
 *
 * Tested with RPi Model B rev2, and an 26bit Wiegand Cards
 * Spiros Ioannou 2017
 *
 */

/* Wiegand Door Access Control
 * 1) Reads Wiegand IDs from Redis
 * 2) Triggers relay according to .ini configuration
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <hiredis.h>
#include <minIni.h>
#include <sys/types.h>
#include <syslog.h>



#include <pirdac.h>


redisReply *reply;
redisContext *redis_context=NULL;
char section[40];
char logmsg[512];

struct option_s {
    int debug;
	char opendoor[512];
	char reader_id[64];
	char conf_file[1024];
} options;



void getTimeDay(char *time_str, char *day_no) ;

int checkIDData(unsigned long id_code);

int redis_connect_loop() {
	int connected=0;

	if (redis_context != NULL)
		redisFree(redis_context);
	while (!connected) {
		redis_context = redisConnect("127.0.0.1", 6379);
		if (redis_context == NULL || redis_context->err) {
			if (redis_context) {
				snprintf(logmsg,sizeof(logmsg),"Error connecting: %s", redis_context->errstr);
				syslog (LOG_ERR,"%s",logmsg);
				redisFree(redis_context);
			} else {
				snprintf(logmsg, sizeof(logmsg), "Can't allocate redis context");
				syslog (LOG_ERR,"%s",logmsg);
			}
			snprintf(logmsg, sizeof(logmsg), "connect failed, retrying..");
			syslog (LOG_ERR,"%s",logmsg);
			sleep (1);
		}
		else {
			connected=1;
			syslog (LOG_INFO,"Connected to redis");
		}
	}
	return 0;
}

/* Subscribe to channel, reconnect if needed */
int redis_subscribe_loop(char * channel) {
	int r,justconnected=1;
	unsigned long int wiegand_code=0;

	while(1) {
		if (justconnected) {
			reply = redisCommand(redis_context, "SUBSCRIBE %s", channel);
			if (options.debug)
				fprintf(stderr, "Subscribed to redis channel [%s]\n",channel);
			snprintf(logmsg, sizeof(logmsg), "Subscribed to redis channel : %s", channel);
			syslog (LOG_INFO,"%s",logmsg);

			freeReplyObject(reply);
			justconnected=0;
		}

		r = redisGetReply(redis_context,(void **)&reply);
		if (r !=  REDIS_OK) {
			//fprintf(stderr,"Context:%d",(int)redis_context);
			redis_connect_loop();
			justconnected=1;
			continue;
		}
		if (reply->type == REDIS_REPLY_ERROR) {
			snprintf(logmsg,sizeof(logmsg), "ERROR: %s", reply->str);
			syslog (LOG_ERR,"%s",logmsg);
		}
		else if (reply->type == REDIS_REPLY_STATUS) {
			snprintf(logmsg, sizeof(logmsg), "STATUS CHANGE: %s", reply->str);
			syslog (LOG_INFO,"%s",logmsg);
		}
		else if (reply->type == REDIS_REPLY_STRING) {
			snprintf(logmsg, sizeof(logmsg), "Got String: %s", reply->str);
			syslog (LOG_INFO,"%s",logmsg);
		}

		/* We received a new message, probably an RFID */
		if (reply->type == REDIS_REPLY_ARRAY) {
			sscanf(reply->element[2]->str, "\"%lu", &wiegand_code); //element contains quotes
			if (options.debug) {
				printf("Got from redis: [%s]\n", reply->element[2]->str);
				printf("Got from redis (int): [%lu]\n", wiegand_code);
			}
			r = checkIDData(wiegand_code);
			if (r==0) { //authorized, now trigger relay
				snprintf(logmsg, sizeof(logmsg), "Opening Door for ID:%lu",wiegand_code);
				syslog (LOG_NOTICE,"%s",logmsg);
				if (options.debug) 
					printf("Opening door with script:[%s]\n",options.opendoor);
				system(options.opendoor);
				sleep(1);
			}
		}

		freeReplyObject(reply);
	}
	return -1;
}

void show_usage(char *pname) {
    printf("PIRDAC - Door Access Control Daemon (https://bitbucket.org/sivann/pirdac)\n");
    printf("Usage: %s [-d] [-o <opendoor script>] -i <reader id> -c <configuration file>\n",pname);
    printf("\t-d\t\tdebug\n");
    printf("\t-i\t\treader id. Redis channel to subscribe matches reader-<reader_id>.\n");
    printf("\t-o\t\tscript to call after successfull auth. Reader id will be supplied as first argument.\n");
    printf("\t-c\t\tuser configuration, usually pirdac_users.ini\n");
    printf("\n");
}

int main(int argc, char *argv[]) {
	int opt;

	// Defaults
    options.debug = 0;
    options.reader_id[0] = 0;
    options.opendoor[0] = 0;
    options.conf_file[0] = 0;
	strncpy(options.opendoor, OPENDOOR, sizeof(options.opendoor));

    /* Parse Options */
    while ((opt = getopt(argc, argv, "do:i:")) != -1) {
        switch (opt) {
        case 'd':
            options.debug++;
            break;
        case 'o':
			strncpy(options.opendoor, optarg, sizeof(options.opendoor));
            break;
        case 'c':
			strncpy(options.conf_file, optarg, sizeof(options.conf_file));
            break;
        case 'i':
			snprintf(options.reader_id, sizeof(options.reader_id),"reader-%s",optarg);
            break;
        case 'h':
            show_usage(argv[0]);
            exit(0);
            break;
        default:               /* '?' */
            show_usage(argv[0]);
            exit(EXIT_FAILURE);
        }
    }

	if (!strlen(options.reader_id)) {
		show_usage(argv[0]);
		syslog (LOG_ERR,"Please specify reader_id");
		fprintf(stderr, "Please specify reader-id\n");
		exit(EXIT_FAILURE);
	}

	if (!strlen(options.conf_file)) {
		show_usage(argv[0]);
		syslog (LOG_ERR,"Please specify configuration file");
		fprintf(stderr, "Please specify configuration file\n");
		exit(EXIT_FAILURE);
	}

    openlog (argv[0], LOG_PID|LOG_PERROR , LOG_AUTH);
	syslog (LOG_INFO, "Started by uid %d", getuid () );

	strcat(options.opendoor," ");
	strcat(options.opendoor,options.reader_id);

	/* connect to redis */
	redis_connect_loop();

	/* subscribe to redis channel and process submissions */
	redis_subscribe_loop(options.reader_id);

	syslog (LOG_ERR,"After subscribe_loop, shouldn't reach this");
	return 0;

}




int checkIDData(unsigned long id_code) {
	int r, r1;
	char s[128];
	char name[128];
	char day_no[2];
	char time_str[16];
	char ID[64];

	snprintf(ID, 128, "%lu", id_code);

	getTimeDay(time_str,day_no);

	if (options.debug) {
		printf("checkIDData: ID: [%s]\n",ID);
		printf("checkIDData: Current time: \"%s\" , day:%s\n", time_str, day_no);
	}

	snprintf(logmsg,sizeof(logmsg),"checkIDData: checking ID:[%s], time:[%s], day:[%s]", ID, time_str, day_no);
	syslog (LOG_NOTICE,"%s",logmsg);

	name[0]=0;
	s[0]=0;

	r = ini_gets(ID,"name",0,name,128, options.conf_file);

	// START
	r = ini_gets(ID,"start",0,s,128, options.conf_file);
	if (r > 0) {
		//printf("Start: %s\n",s);
		r1 = strncmp(s, time_str,16);
		if (r1 > 0) {
			snprintf(logmsg,sizeof(logmsg),"ID:[%s], name[%s]: Start time [%s] not reached yet, time now: [%s]",ID, name, s, time_str);
			syslog (LOG_NOTICE,"%s",logmsg);
			return -1;
		}

	}
	else {
		snprintf(logmsg, sizeof(logmsg), "start key empty or missing from section, or section missing:%s",ID);
		syslog (LOG_ERR,"%s",logmsg);
		return -1;
	}


	// END
	r = ini_gets(ID,"end",0,s,128, options.conf_file);
	if (r > 0) {
		r1 = strncmp(s, time_str,16);
		if (r1 < 0) {
			snprintf(logmsg,sizeof(logmsg), "ID:[%s], name:[%s]: End time [%s] passed, time now: [%s]", ID, name, s, time_str);
			syslog (LOG_NOTICE,"%s",logmsg);
			return -1;
		}

	}
	else {
		snprintf(logmsg,sizeof(logmsg), "end key empty or missing from section:%s",ID);
		syslog (LOG_ERR,"%s",logmsg);
		return -1;
	}

	// WEEKDAY
	r = ini_gets(ID,"weekdays",0,s,128, options.conf_file);
	if (r > 0) {
		if (!strstr(s, day_no)) {
			snprintf(logmsg,sizeof(logmsg),"ID:[%s], name:[%s]: current Weekday [%s], not in allowed weekday list [%s]",ID, name, day_no, s);
			syslog (LOG_NOTICE,"%s",logmsg);
			return -1;
		}

	}
	else {
		snprintf(logmsg,sizeof(logmsg), "weekdays key empty or missing from section:%s",ID);
		syslog (LOG_NOTICE,"%s",logmsg);
		return -1;
	}


	r = ini_gets(ID,"name",0,s,128, options.conf_file);
	snprintf(logmsg, sizeof(logmsg), "Authorized user [%s] with ID [%s], time:[%s], day:[%s]",s, ID, time_str, day_no);
	syslog (LOG_NOTICE,"%s",logmsg);


	return 0;

}

void getTimeDay(char *time_str, char *day_no) {
	time_t t;
	struct tm *tm_p;

	t = time(NULL);
	tm_p = localtime(&t);
	if (tm_p == NULL) {
		snprintf(logmsg,sizeof(logmsg), "localtime:%s",strerror(errno));
		syslog (LOG_ERR,"%s",logmsg);
	}

	if (strftime(day_no, 2, "%u", tm_p) == 0) {
	    snprintf(logmsg,sizeof(logmsg), "strftime:%s",strerror(errno));
		syslog (LOG_ERR,"%s",logmsg);
	}

	if (strftime(time_str, 16, "%H:%M:%S", tm_p) == 0) {
	    snprintf(logmsg, sizeof(logmsg), "strftime:%s",strerror(errno));
		syslog (LOG_ERR,"%s",logmsg);
	}

}
