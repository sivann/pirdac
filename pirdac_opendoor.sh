#!/bin/bash
export PATH=$PATH:.

#Find door GPIO based on related wiegand reader id
READERID2DOORGPIO[reader-0]=1
READERID2DOORGPIO[reader-2]=3

readerid="$1"
gpio=${DOOR2GPIO[$readerid]}

pirdac_opendoor -s 3 -p $gpio
