/* Trigger an (active low) relay on GPIO, similar to this: https://www.sainsmart.com/arduino-pro-mini.html */
/* gcc opendoor.c -lwiringPi */


#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <memory.h>
#include <stdint.h>
#include <sys/time.h>
#include <signal.h>
#include <syslog.h>
#include <wiringPi.h>

/* Defaults, change with command-line options */
#define RELAY_PIN 1

void show_usage() {
    printf("PIRDAC - opendoor - trigger the door relay\n");
    printf("Usage: opendoor  [-s trigger_seconds] [-p GPIO_pin]\n");
    printf("\t-s\t\tseconds for the relay to stay triggered (default 3)\n");
    printf("\t-p\t\tGPIO pin connected to relay (default 1)\n");
    printf("\n");
}

int main(int argc, char ** argv) {
    int opt;
	int relay_pin=RELAY_PIN;
	int sleep_sec=3;


    /* Parse Options */
    while ((opt = getopt(argc, argv, "p:s:")) != -1) {
        switch (opt) {
        case 'p':
            relay_pin = atoi(optarg);
            break;
        case 's':
            sleep_sec = atoi(optarg);
            break;
        case 'h':
            show_usage(argv[0]);
            exit(0);
            break;
        default:               /* '?' */
            show_usage(argv[0]);
            exit(EXIT_FAILURE);
        }
    }

	openlog (argv[0], LOG_PID|LOG_PERROR , LOG_AUTH);
	syslog (LOG_INFO, "Started by uid %d", getuid () );


    wiringPiSetup();

	pinMode(relay_pin, OUTPUT);

	syslog (LOG_INFO, "Activating relay (pin->LOW)");
	digitalWrite (relay_pin, LOW) ;
	//printf("Low\n");
	syslog (LOG_INFO, "Waiting %d seconds",sleep_sec);
	sleep(sleep_sec);

	syslog (LOG_INFO, "Deactivating relay (pin->HIGH)");
	digitalWrite (relay_pin, HIGH) ;
	syslog (LOG_INFO, "Finished");
	digitalWrite (relay_pin, HIGH) ;

	exit(0);

}
